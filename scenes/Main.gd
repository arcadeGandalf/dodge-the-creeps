extends Node2D

export (PackedScene) var Mob
export (PackedScene) var Missile
var score
var missiles_active = 0

# Called when the node enters the scene tree for the first time.
func _ready():
	randomize()	
	$MenuMusic.play()

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass

func _on_Player_hit():
	$HUD.update_health($Player.health)
	$PlayerHitSound.play()

func _on_Player_dead():
	game_over()
	
func game_over():
	$Music.stop()
	$DeathSound.play()
	$MenuMusic.play()
	$ScoreTimer.stop()
	$MobTimer.stop()	
	$HUD.show_game_over()
	$HUD.hide_game_elements()
	
	
func new_game():
	$MenuMusic.stop()
	$Music.play()
	score = 0
	$Player.start($StartPosition.position)
	$StartTimer.start()
	
	$HUD.update_score(score)
	$HUD.show_message("Get Ready")
	$HUD.update_health($Player.health)
	$HUD.show_game_elements()

func _on_StartTimer_timeout():
	$MobTimer.start()
	$ScoreTimer.start()
	
func _on_ScoreTimer_timeout():
	score += 1
	
	$HUD.update_score(score)

func _on_MobTimer_timeout():
	# Choose a random location on Path2D.
	$MobPath/MobSpawnLocation.offset = randi()
	# Create a Mob instance and add it to the scene.
	var mob = Mob.instance()
	add_child(mob)
	# Set the mob's direction perpendicular to the path direction.
	var direction = $MobPath/MobSpawnLocation.rotation + PI / 2
	# Set the mob's position to a random location.
	mob.position = $MobPath/MobSpawnLocation.position
	# Add some randomness to the direction.
	direction += rand_range(-PI / 4, PI / 4)
	mob.rotation = direction
	# Set the velocity (speed & direction).
	mob.linear_velocity = Vector2(rand_range(mob.min_speed, mob.max_speed), 0)
	mob.linear_velocity = mob.linear_velocity.rotated(direction)
	$HUD.connect("start_game", mob, "_on_start_game")
	
func _on_Player_fire_missile():
	if missiles_active < 1:
		missiles_active = 1
		var missile = Missile.instance()
		missile.connect("missile_used", self, "_on_Missile_used")
		add_child(missile)
		$MissilePath/MissileSpawnLocation.offset = randi()
		var direction = $MobPath/MobSpawnLocation.rotation + PI / 2	
		missile.position = $Player.position
		direction += rand_range(-PI / 4, PI / 4)
		missile.rotation = $Player.current_direction
		# Set the velocity (speed & direction).
		missile.linear_velocity = Vector2(rand_range(missile.min_speed, missile.max_speed), 0)
		missile.linear_velocity = missile.linear_velocity.rotated($Player.current_direction)				
		$Pew.play()		

func _on_HUD_start_game():
	new_game()

func _on_Missile_used():
	missiles_active = 0
