extends RigidBody2D

signal missile_used

export var min_speed = 600 # Minimum speed range.
export var max_speed = 800 # Maximum speed range.

# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass


func _on_VisibilityNotifier2D_screen_exited():
	queue_free()
	emit_signal("missile_used")


func _on_Missile_body_entered(body):
	queue_free()
	emit_signal("missile_used")
