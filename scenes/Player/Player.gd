extends Area2D

signal hit
signal dead
signal fire_missile

export var speed = 400  # How fast the player will move (pixels/sec)
export var current_direction = 0
export var health = 3
var screen_size # size of the game window
var alive = true
var default_health = 3

# Called when the node enters the scene tree for the first time.
func _ready():
	screen_size = get_viewport_rect().size
	hide()
	
func start(pos):
	position = pos
	show()
	$CollisionShape2D.disabled = false
	alive = true
	health = default_health


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	
	# handle player movement
	var velocity = Vector2() # the player's movement vector
	if Input.is_action_pressed("ui_right"):
		velocity.x += 1
		current_direction = 0
	if Input.is_action_pressed("ui_left"):
		velocity.x -= 1
		current_direction = 3.15
	if Input.is_action_pressed("ui_down"):
		velocity.y += 1
		current_direction = 1.58
	if Input.is_action_pressed("ui_up"):
		velocity.y -= 1
		current_direction = 4.7
	
	# control animation
	if velocity.length() > 0:
		velocity = velocity.normalized() * speed
		$AnimatedSprite.play()
		$Trail.emitting = true
	else:
		$AnimatedSprite.stop()
		$Trail.emitting = false
	
	# handle player position
	position += velocity * delta
	position.x = clamp(position.x, 0, screen_size.x)
	position.y = clamp(position.y, 0, screen_size.y)	
	
	# handle animation used
	if velocity.x != 0:
		$AnimatedSprite.animation = "right"
		$AnimatedSprite.flip_v = false
		$AnimatedSprite.flip_h = velocity.x < 0
	elif velocity.y != 0:
		$AnimatedSprite.animation = "up"
		$AnimatedSprite.flip_v = velocity.y > 0


func _on_Player_body_entered(body):	
	var name = body.get_name()
	if "Mob" in name and body.is_threat:						
		health -= 1	
		emit_signal("hit")		
		
		if health == 0:
			$CollisionShape2D.set_deferred("disabled", true)
			alive = false
			hide()
			emit_signal("dead")						
		
func _input(event):
	if Input.is_action_pressed("ui_trigger"):
		if alive:
			emit_signal("fire_missile")
