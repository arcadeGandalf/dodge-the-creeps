extends CanvasLayer


signal start_game

# Called when the node enters the scene tree for the first time.
func _ready():
	hide_game_elements()
	
func hide_game_elements():
	$HealthLabel.hide()
	$HealthImage.hide()
	$ScoreLabel.hide()
	
func show_game_elements():
	$HealthLabel.show()
	$HealthImage.show()
	$ScoreLabel.show()

func show_message(text):
	$MessageLabel.text = text
	$MessageLabel.show()
	$MessageTimer.start()
	
func show_game_over():
	show_message("Game Over")
	
	yield($MessageTimer, "timeout")	
	
	$MessageLabel.text = "Dodge the \nCreeps!"
	$MessageLabel.show()
	
	yield(get_tree().create_timer(1), "timeout")
	
	$StartButton.show()
	
func update_score(score):
	$ScoreLabel.text = str(score)
# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass

func update_health(health):
	$HealthLabel.text = str(health)

func _on_StartButton_pressed():
	$StartButton.hide()
	emit_signal("start_game")

func _on_MessageTimer_timeout():
	$MessageLabel.hide()	

